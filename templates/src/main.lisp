;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
;;;; main.lisp

(in-package #:{{project_name}})

(defun hello ()
  (format t "~A" '(1 2)))

(hello)
