# Project-create

A simple lisp project to generate quickly a customized project scaffold.

# Author

+ Dan Loaiza <papachan@gmail.com>

# Copyright

Copyright (c) 2018 Dan Loaiza <papachan@gmail.com>

# License

Licensed under the BSD License.
